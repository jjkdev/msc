var os = require('os')
var cli = require('cli');
var util = require('util');

var cluster = require('cluster');
var memored = require('memored');

var VoltClient = require('./lib/client');
var VoltConfiguration = require('./lib/configuration');
var VoltProcedure = require('./lib/query');
var VoltQuery = require('./lib/query');

var numCPUs = os.cpus().length;

//  Benchmark
var totalOperations = 0;
var totalTime = 0;

//  Procedures
var readAllProc = new VoltProcedure('ReadAll', ['int', 'int']);
var read1WProc = new VoltProcedure('Read1W', ['double']); //Price
var read2WProc = new VoltProcedure('Read2W', ['double', 'long']); //Price, Date
var read3WProc = new VoltProcedure('Read3W', ['double', 'long', 'int']); //Price, Date, ShopID
var read1WJProc = new VoltProcedure('Read1WJ', ['string']); //ShopName(ShopID)
var read2WJProc = new VoltProcedure('Read2WJ', ['string', 'string']); //ShopName(ShopID), ManufacturerName(ProductID)
var read3WJProc = new VoltProcedure('Read3WJ', ['string', 'string', 'string']); //ShopName(ShopID), ManufacturerName(ProductID), CategoryName(CategoryID)
var writeProc = new VoltProcedure('Write', ['int', 'string']); //ShopID, ShopName
var deleteProc = new VoltProcedure('Delete', ['int']); //OfferID
var deleteAllProc = new VoltProcedure('DeleteAll');
var updateProc = new VoltProcedure('Update', ['double', 'int']); //price, <= id

var client = null;

const options = cli.parse({
  loops : ['t', 'Number of loops to run', 'number', 10000],
  operation : ['o', 'Operation to run: exit, read_all, read1w, read2w, read3w, read1wj, read2wj, read3wj, write, delete, deletex, delete_all', 'string', 'exit'],
  voltGate : ['h', 'VoltDB host (any if multi-node)', 'string', 'localhost'],
  workers : ['c', 'Number of clients (web workers); CPU Cores set as default', 'number', numCPUs],
  verbose : ['v', 'verbose output'],
  debug : ['d', 'debug output']
});

var workers = options.workers;

//Multithreading
if( cluster.isMaster ) master_main();
else worker_main();

function master_main()
{
  //console.log("VoltDB host:  " + options.voltGate);
  //console.log("Workers: " + workers);

  for(var i = 0; i < workers; i++)
  {
    var worker = cluster.fork();

    worker.on('message', function(msg) {
      if(msg.cmd && msg.cmd == 'resultTotalTime') totalTime += msg.totalTime;
    });

    worker.on('message', function(msg) {
      if(msg.cmd && msg.cmd == 'resultTotalOperations') totalOperations += msg.totalOperations;
    });
  }

  var startTime = new Date().getTime();

  var exited = 0;
  cluster.on('exit', function(worker) {

    //console.log("Closing Worker( #"+worker.pid+" )");
    ++exited;

    if(exited == workers)
    {
      var total = new Date().getTime() - startTime;

      console.log('Total Operations: ' + totalOperations);
      console.log('');
      console.log('Operation Time [ms]: ' + totalTime/totalOperations/options.workers);
      console.log('');
      console.log('Total Time (app lifetime): ' + total);
      console.log('TPS: ' +  (totalOperations/total*1000) );
      console.log('');
      console.log('Total Time (true) (ms): ' + (totalTime/options.workers));
      console.log('TPS (true): ' +  (totalOperations/(totalTime/options.workers)*1000) );
    }

  });

}

function worker_main()
{
  // process.env.NODE_WORKER_ID

  client = new VoltClient([{
    host : options.voltGate,
    port : 21212,
    username : 'root',
    password : 'root',
    service : 'database',
    queryTimeout : 50000
  }]);


   client.connect(function startup(results) {
     var startTime;
     //console.log('Connected ( #'+process.pid+' ); Loops left: ' + options.loops);

     var price = rand(100,2000);
     var offer_id = rand(100,2000);
     var shop_id = 312;
     var date = new Date('2017-01-01').valueOf();
     var shop_name = 'RTV EURO AGD';
     var category_name = 'Ekspresy do kawy';
     var manufacturer_name = 'De Longhi';

     var operationsDone = 0;
     var dividor = 1000;
     if(options.loops <= 1250) dividor = 25;

     startTime = new Date().getTime();
     // BEGIN: COMMANDS
     switch( options.operation )
     {
     case 'read_all':

       startTime = new Date().getTime();
       for(i = 0; i < options.loops/dividor; i++)
       {
          var offset = i * options.loops/dividor;
          var limit = dividor;

          var query = readAllProc.getQuery();
          query.setParameters([ limit, offset ]);

          client.callProcedure(query, function queryResult(event, code, results)
          {

            //console.log(results.table[0].length);
            if( results.error == true ) console.log('UndefinedError');

            ++operationsDone;


            if( operationsDone == options.loops/dividor )
            {
              logTime( startTime, options.loops );
              process.exit();
            }

          }, function loginError(results) {
            console.log('Node did not connect to VoltDB');
          });
        }

     break;
     case 'read1w':

       for(i = 0; i < options.loops; i++)
       {
          let query = read1WProc.getQuery()
          query.setParameters([ price ])

          client.callProcedure(query, function queryResult(event, code, results){

            if( results.error == true ) console.log('UndefinedError')

            ++operationsDone

            if( operationsDone == options.loops )
            {
              logTime( startTime, options.loops )
              process.exit()
            }

          });
        }

     break;
     case 'read2w':

       for(i = 0; i < options.loops; i++)
       {
          var query = read2WProc.getQuery();
          query.setParameters([ price, date ]);
          client.callProcedure(query, function queryResult(event, code, results){

            if( results.error == true ) console.log('UndefinedError');

            ++operationsDone;
            if( operationsDone == options.loops )
            {
              logTime( startTime, options.loops );
              process.exit();
              //console.log(results.table[0]);
            }

          });
        }

     break;
     case 'read3w':

       for(i = 0; i < options.loops; i++)
       {
          var query = read3WProc.getQuery();
          query.setParameters([ price, date, shop_id ]);
          client.callProcedure(query, function queryResult(event, code, results){

            if( results.error == true ) console.log('UndefinedError');

            ++operationsDone;
            if( operationsDone == options.loops )
            {
              logTime( startTime, options.loops );
              process.exit();
            }

          });
       }

     break;
     case 'read1wj':

       for(i = 0; i < options.loops; i++)
       {
          var query = read1WJProc.getQuery();
          query.setParameters([ shop_name ]);
          client.callProcedure(query, function queryResult(event, code, results){

            if( results.error == true ) console.log('UndefinedError');
            //console.log(results.table[0]);
            ++operationsDone;
            if( operationsDone == options.loops )
            {
              logTime( startTime, options.loops );
              process.exit();
              //console.log(results.table[0]);
            }

          });
       }

     break;
     case 'read2wj':

       for(i = 0; i < options.loops; i++)
       {
          var query = read2WJProc.getQuery();
          query.setParameters([ shop_name, manufacturer_name ]);
          client.callProcedure(query, function queryResult(event, code, results){

            if( results.error == true ) console.log('UndefinedError');

            ++operationsDone;
            if( operationsDone == options.loops )
            {
              logTime( startTime, options.loops );
              process.exit();
              //console.log(results.table[0]);
            }

          });
       }

     break;
     case 'read3wj':

       for(i = 0; i < options.loops; i++)
       {
          var query = read3WJProc.getQuery();
          query.setParameters([ shop_name, manufacturer_name, category_name ]);
          client.callProcedure(query, function queryResult(event, code, results){

            if( results.error == true ) console.log('UndefinedError');

            ++operationsDone;
            if( operationsDone == options.loops )
            {
              logTime( startTime, options.loops );
              process.exit();
              //console.log(results.table[0]);
            }

          });
       }

     break;
     case 'write':

        var shop_id_iter = 100000;

        for(i = 0; i < options.loops; i++)
        {
          var cur_shop_id = shop_id_iter + i;
          var query = writeProc.getQuery();
          query.setParameters([ cur_shop_id, 'TEST' ]);
          client.callProcedure(query, function queryResult(event, code, results){

            if( results.error == true ) console.log('UndefinedError');

            ++operationsDone;
            if( operationsDone == options.loops )
            {
              logTime( startTime, options.loops );
              process.exit();
              //console.log(results.table[0]);
            }

          });
        }

     break;
     case 'delete':

       var query = deleteProc.getQuery();
       query.setParameters([ options.loops ]);
       client.callProcedure(query, function queryResult(event, code, results){

         if( results.error == true ) console.log('UndefinedError');

         logTime( startTime, options.loops );
         process.exit();
         //console.log(results.table[0]);

       });

     break;
     case 'deletex':

       for(i = 0; i < options.loops; i++)
       {
          var query = deleteProc.getQuery();
          query.setParameters([ 1 ]);
          client.callProcedure(query, function queryResult(event, code, results){

            if( results.error == true ) console.log('UndefinedError');

            ++operationsDone;
            if( operationsDone == options.loops )
            {
              logTime( startTime, options.loops );
              process.exit();
              //console.log(results.table[0]);
            }

          });
        }

     break;
     case 'delete_all':

       var query = deleteAllProc.getQuery();
       client.callProcedure(query, function queryResult(event, code, results){

         if( results.error == true ) console.log('UndefinedError');

         logTime( startTime, options.loops );
         process.exit();

       });

     break;
     case 'update':

     var price = 1.11;
     var id = options.loops;

     var query = updateProc.getQuery();
     query.setParameters([ price, id ]);
     client.callProcedure(query, function queryResult(event, code, results){

       if( results.error == true ) console.log('UndefinedError');

       logTime( startTime, options.loops );
       process.exit();

     });

     break;
     }
     // END: COMMANDS
     //logTime( startTime, options.loops );

     //process.exit();
   }, function loginError(results) {
     console.log('loginError' + results);
   });
}

function logTime( startTime, transactions )
{

  var endTime = new Date().getTime();
  var totalTime = endTime - startTime;

  process.send({
    cmd : 'resultTotalTime',
    totalTime : totalTime
  });

  if(transactions > 0)
  {
    process.send({
      cmd : 'resultTotalOperations',
      totalOperations : transactions
    });
  }
}

function rand(min, max) { return Math.floor(Math.random() * (Math.floor(max) - Math.ceil(min))) + Math.ceil(min); }
