var os = require('os')
var cli = require('cli');
var util = require('util');

var cluster = require('cluster');
var memored = require('memored');

const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

// Connection URL
const url = 'mongodb://localhost:27017';

// Database Name
const dbName = 'benchmark';

//  Benchmark
var totalOperations = 0;
var totalTime = 0;

const options = cli.parse({
  loops : ['t', 'Number of loops to run', 'number', 1],
  operation : ['o', 'Operation to run: exit, read_all, read1w, read2w, read3w, read1wj, read2wj, read3wj, write, delete, deletex, delete_all', 'string', 'exit']
});

master_main();

function master_main()
{

  var startTime;

  var price = rand(100,2000);
  var offer_id = rand(100,2000);
  var shop_id = 312;
  var date = '2017-01-01';
  var shop_name = 'RTV EURO AGD';
  var category_name = 'Ekspresy do kawy';
  var manufacturer_name = 'De Longhi';

  var operationsDone = 0;

  MongoClient.connect(url, { useNewUrlParser: true, poolSize: options.workers }, function(err, db) {
    if (err) throw err;
    var dbo = db.db( dbName );
    // BEGIN: COMMANDS
    switch( options.operation )
    {
    case 'read_all':

      startTime = new Date().getTime();
      for(i = 0; i < options.loops; i++)
      {
          dbo.collection("offers").find({}, function(err, result) {
            if (err) throw err;
            ++operationsDone;
            if( operationsDone == options.loops )
            {
              var endTime = new Date().getTime();
              results( startTime, endTime );
              db.close();
            }
          });
       }

    break;
    case 'read1w':
      startTime = new Date().getTime();
      for(i = 0; i < options.loops; i++)
      {
          dbo.collection("offers").find({ price: { $gt: price } }, function(err, result) {
            if (err) throw err;
            ++operationsDone;
            if( operationsDone == options.loops )
            {
              var endTime = new Date().getTime();
              results( startTime, endTime );
              db.close();
            }
          });
      }

    break;
    case 'read2w':
      startTime = new Date().getTime();
      for(i = 0; i < options.loops; i++)
      {
        dbo.collection("offers").find({ $and : [
          { price : { $gt : price } },
          { date : { $gt : date } }
        ] }, function(err, result) {
          if (err) throw err;
          ++operationsDone;
          if( operationsDone == options.loops )
          {
            var endTime = new Date().getTime();
            results( startTime, endTime );
            db.close();
          }
        });
      }

    break;
    case 'read3w':
      startTime = new Date().getTime();
      for(i = 0; i < options.loops; i++)
      {
        dbo.collection("offers").find({ $and : [
          { price : { $gt : price } },
          { date : { $gt : date } },
          { ref_shop : shop_id }
        ] }, function(err, result) {
          if (err) throw err;
          ++operationsDone;
          if( operationsDone == options.loops )
          {
            var endTime = new Date().getTime();
            results( startTime, endTime );
            db.close();
          }
        });
      }

    break;
    case 'read1wj':
      startTime = new Date().getTime();
      for(i = 0; i < options.loops; i++)
      {
        dbo.collection("offers").aggregate(
          [
            {
              $lookup:
               {
                 from: 'shops',
                 localField: 'ref_shop',
                 foreignField: 'id',
                 as: 'availableIn'
               }
             },
            {$unwind:"$availableIn"},
            {$match:{"availableIn.name":shop_name}}
          ]).toArray(function(err, result) {
          if (err) throw err;
          ++operationsDone;
          if( operationsDone == options.loops )
          {
            var endTime = new Date().getTime();
            results( startTime, endTime );
            db.close();
          }
        });
      }

    break;
    case 'read2wj':
      startTime = new Date().getTime();
      for(i = 0; i < options.loops; i++)
      {
        dbo.collection("offers").aggregate(
          [
            {
              $lookup:
               {
                 from: 'shops',
                 localField: 'ref_shop',
                 foreignField: 'id',
                 as: 'availableIn'
               }
             },
            {$unwind:"$availableIn"},
            {$match:{"availableIn.name":shop_name}},
            {
              $lookup:
               {
                 from: 'products',
                 localField: 'ref_product',
                 foreignField: 'id',
                 as: 'forProduct'
               }
             },
            {$unwind:"$forProduct"},
            {$match:{"forProduct.manufacturer_name":manufacturer_name}}
          ]).toArray(function(err, result) {
          if (err) throw err;
          ++operationsDone;
          if( operationsDone == options.loops )
          {
            var endTime = new Date().getTime();
            results( startTime, endTime );
            db.close();
          }
        });
      }

    break;
    case 'read3wj':
      startTime = new Date().getTime();
      for(i = 0; i < options.loops; i++)
      {
        dbo.collection("offers").aggregate(
          [
            {
              $lookup:
               {
                 from: 'shops',
                 localField: 'ref_shop',
                 foreignField: 'id',
                 as: 'availableIn'
               }
             },
            {$unwind:"$availableIn"},
            {$match:{"availableIn.name":shop_name}},
            {
              $lookup:
               {
                 from: 'products',
                 localField: 'ref_product',
                 foreignField: 'id',
                 as: 'forProduct'
               }
             },
            {$unwind:"$forProduct"},
            {$match:{"forProduct.manufacturer_name":manufacturer_name}},
            {
              $lookup:
               {
                 from: 'categories',
                 localField: 'ref_category',
                 foreignField: 'id',
                 as: 'inCategory'
               }
             },
            {$unwind:"$inCategory"},
            {$match:{"inCategory.name":category_name}}
          ]).toArray(function(err, result) {
          if (err) throw err;
          ++operationsDone;
          if( operationsDone == options.loops )
          {
            var endTime = new Date().getTime();
            results( startTime, endTime );
            db.close();
          }
        });
      }

    break;
    case 'write':

       var shop_id_iter = 100000;
       startTime = new Date().getTime();
       for(i = 0; i < options.loops; i++)
       {
         var cur_shop_id = shop_id_iter + i;
         var shopObj = { id: cur_shop_id, name: "TEST" };

         dbo.collection("shops").insertOne(shopObj, function(err, result) {

           if (err) throw err;

           ++operationsDone;
           if( operationsDone == options.loops )
           {
             var endTime = new Date().getTime();
             results( startTime, endTime );
             db.close();
           }
         });
       }

    break;
    case 'delete':
    startTime = new Date().getTime();
    dbo.collection("offers").deleteMany({id: { $lt: options.loops }}, function(err, result) {
      if (err) throw err;

      var endTime = new Date().getTime();
      results( startTime, endTime );
      db.close();
    });


    break;
    case 'deletex':
      startTime = new Date().getTime();
      for(i = 0; i < options.loops; i++)
      {
        dbo.collection("offers").deleteOne({}, function(err, result) {
          if (err) throw err;

          ++operationsDone;
          if( operationsDone == options.loops )
          {
            var endTime = new Date().getTime();
            results( startTime, endTime );
            db.close();
          }
        });
      }

    break;
    case 'update':

      dbo.collection("shops").find({}).toArray(function(err, result) {

        if (err) throw err;

        let shop_ids = [];
        result.forEach(function ( row ) {
          shop_ids.push( row.id )
        });

        startTime = new Date().getTime();
        for(i = 0; i < options.loops; i++)
        {
          let shop_id = shop_ids[Math.floor(Math.random()*shop_ids.length)];

          let query = { id: shop_id };
          let changeTo = { $set: {name: "TEST" } };

          dbo.collection("shops").updateOne(query, changeTo, function(err, res) {
            if (err) throw err;

            ++operationsDone;
            if( operationsDone == options.loops )
            {
              var endTime = new Date().getTime();
              results( startTime, endTime );

              db.close();
            }
          });
        }

      });



    break;
    }

    // /process.exit();
  });

}

function logTime( startTime )
{
  var endTime = new Date().getTime();
  var totalTime = endTime - startTime;

  return totalTime;
}

function results( startTime, endTime )
{
  var totalTime = endTime - startTime;

  console.log('Total Operations: ' + options.loops);
  console.log('');
  console.log('Total Time: ' + totalTime);
  console.log('TPS (true): ' +  (options.loops/totalTime*1000) );
}

function rand(min, max) { return Math.floor(Math.random() * (Math.floor(max) - Math.ceil(min))) + Math.ceil(min); }
