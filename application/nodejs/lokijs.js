var os = require('os');
var fs = require('fs');
var cli = require('cli');
var util = require('util');

var _ = require('lodash');

var cluster = require('cluster');
var memored = require('memored');

const loki = require('lokijs');
const lfsa = require('lokijs/src/loki-fs-structured-adapter')
const papa = require('papaparse');

var numCPUs = os.cpus().length;

//  Benchmark
var totalOperations = 0;
var totalTime = 0;

var client = null;

const options = cli.parse({
  loops : ['t', 'Number of loops to run', 'number', 10000],
  operation : ['o', 'Operation to run: exit, read_all, read1w, read2w, read3w, read1wj, read2wj, read3wj, write, delete, deletex, delete_all', 'string', 'exit'],
  voltGate : ['h', 'VoltDB host (any if multi-node)', 'string', 'localhost'],
  workers : ['c', 'Number of clients (web workers); CPU Cores set as default', 'number', numCPUs],
  verbose : ['v', 'verbose output'],
  debug : ['d', 'debug output']
});

var workers = options.workers;
var adapter = new lfsa();
var db = new loki('benchmark.db', {
  adapter : adapter,
  autoload: true,
  autoloadCallback : import,
  autosave: true,
  autosaveInterval: 4000
});

function import()
{
  var startTime = new Date().getTime();

  /* Initialize shops */
  var shops = db.getCollection("shop");

  if(shops === null)
  {
    shops = db.addCollection("shop");

    var content = fs.readFileSync('/home/jjkdev/Desktop/dataset/with_headers/shops.csv', "utf8");
    var data = papa.parse( content , {
      quotes: true,
      quoteChar: '"',
      escapeChar: '"',
      delimiter: ",",
      header: true,
      newline: "\n"
    });

    data.data.forEach(function ( row ) {
      shops.insert( row );
    });
  }

  /* Initialize categories */
  var categories = db.getCollection("category");

  if(categories === null)
  {
    categories = db.addCollection("category");

    var content = fs.readFileSync('/home/jjkdev/Desktop/dataset/with_headers/categories.csv', "utf8");
    //console.log(content);
    var data = papa.parse( content , {
      quotes: true,
      quoteChar: '"',
      escapeChar: '"',
      delimiter: ",",
      header: true,
      newline: "\n"
    });

    data.data.forEach(function ( row ) {
      categories.insert( row );
    });

  }
  /* Initialize products */
  var products = db.getCollection("product");

  if(products === null)
  {
    products = db.addCollection("product");

    var content = fs.readFileSync('/home/jjkdev/Desktop/dataset/with_headers/products.csv', "utf8");
    //console.log(content);
    var data = papa.parse( content , {
      quotes: true,
      quoteChar: '"',
      escapeChar: '"',
      delimiter: ",",
      header: true,
      newline: "\n"
    });

    data.data.forEach(function ( row ) {
      products.insert( row );
    });

  }
  /* Initialize offers */
  var offers = db.getCollection("offer");

  if(offers === null)
  {
    offers = db.addCollection("offer");

    var content = fs.readFileSync('/home/jjkdev/Desktop/dataset/with_headers/offers.csv', "utf8");
    //console.log(content);
    var data = papa.parse( content , {
      quotes: true,
      quoteChar: '"',
      escapeChar: '"',
      delimiter: ",",
      header: true,
      newline: "\n"
    });

    data.data.forEach(function ( row ) {
      offers.insert( row );
    });

  }

  var time = new Date().getTime() - startTime;

  if( time > 0 ) console.log('Database Import Time: ' + time + ' ms');
  console.log('Database has been initialized.')

  if( cluster.isMaster ) master_main();
}

function master_main()
{
  for(var i = 0; i < workers; i++)
  {
    var worker = cluster.fork();

    worker.on('message', function(msg) {
      if(msg.cmd && msg.cmd == 'resultTotalTime') totalTime += msg.totalTime;
    });

    worker.on('message', function(msg) {
      if(msg.cmd && msg.cmd == 'resultTotalOperations') totalOperations += msg.totalOperations;
    });
  }

  var startTime = new Date().getTime();
  //console.log('Connected ( #'+process.pid+' ); Loops left: ' + options.loops);

  var price = rand(100, 2000);
  var offer_id = rand(100, 2000);
  var shop_id = 312;
  var date = '2017-01-01';
  var shop_name = 'RTV EURO AGD';
  var category_name = 'Ekspresy do kawy';
  var manufacturer_name = 'De Longhi';

  // BEGIN: COMMANDS
  switch( options.operation )
  {
  case 'read_all':

    startTime = new Date().getTime();

    for(i = 0; i < options.loops; i++)
    {
      var offers = db.getCollection("offer");
      var result = offers.find();

      console.log("Row count: " + result.length);
    }

  break;
  case 'read1w':

    for(i = 0; i < options.loops; i++)
    {
      var offers = db.getCollection("offer");
      var results = offers.chain().find({'price': {'$gt': price} }).data();

      console.log("Row count: " + results.length);
    }

  break;
  case 'read2w':
    for(i = 0; i < options.loops; i++)
    {
      var offers = db.getCollection("offer");
      var results = offers.chain().find({$and: [{'price': {'$gt': price} },{'date': {'$gt': date} }]}).data();
    }
  break;
  case 'read3w':

    for(i = 0; i < options.loops; i++)
    {
      var offers = db.getCollection("offer");
      var results = offers.chain().find({$and: [{'price': {'$gt': price} },{'date': {'$gt': date} },{'ref_shop': {'$eq': shop_id} }]}).data();
    }

  break;
  case 'read1wj':

    var results = JOIN('offer', 'ref_shop', 'shop', 'id');

  break;
  case 'read2wj':

    console.log('Not supported;');

  break;
  case 'read3wj':

    console.log('Not supported;');

  break;
  case 'write':

     var shops = db.getCollection("shop");

     startTime = new Date().getTime();

     for(i = 0; i < options.loops; i++)
     {
       shops.insert({name: 'TEST'+i });
     }
  break;
  case 'update':

     var offers = db.getCollection("offer");
     var result = offers.chain().find({ 'id': { '$ne': null } }).simplesort('id', true).limit(options.loops).data();

     result.forEach(function(element,index){ result[index].price = 1.11; });

     startTime = new Date().getTime();
     result.forEach(function(element,index){ offers.update( result[index] ); });

  break;
  case 'delete':

    var offers = db.getCollection("offer");
    var result = offers.chain().find({ 'id': { '$ne': null } }).simplesort('id', true).limit(options.loops).data();

    startTime = new Date().getTime();
    result.forEach(function(element,index){ offers.remove( result[index] ); });
  break;
  case 'delete_all':

    var offers = db.getCollection("offer");
    offers.chain().remove();

  break;
  }
  // END: COMMANDS

  setImmediate( (result) => { show_result( result ); process.exit(); }, logTime(startTime) );
}

function show_result( totalTime )
{
  console.log('');
  console.log('Operation (ms): ' + totalTime/options.loops);
  console.log('');
  console.log('Total Time: ' + totalTime);
  console.log('Total Operations: ' + options.loops);
  console.log('');
  console.log('TPS (true): ' +  ((options.loops/totalTime)*1000) );
}

function logTime( startTime )
{
  var endTime = new Date().getTime();
  var totalTime = endTime - startTime;

  return totalTime;
}

function rand(min, max) { return Math.floor(Math.random() * (Math.floor(max) - Math.ceil(min))) + Math.ceil(min); }
