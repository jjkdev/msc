let os = require('os');
let fs = require('fs');
let cli = require('cli');
let util = require('util');

let _ = require('lodash');

const alasql = require('alasql');

let numCPUs = os.cpus().length;

//  Benchmark
let totalOperations = 0;
let totalTime = 0;

let client = null;

const options = cli.parse({
  loops : ['t', 'Number of loops to run', 'number', 10000],
  operation : ['o', 'Operation to run: exit, read_all, read1w, read2w, read3w, read1wj, read2wj, read3wj, write, delete, deletex, delete_all', 'string', 'exit'],
  workers : ['c', 'Number of clients (web workers); CPU Cores set as default', 'number', numCPUs],
  verbose : ['v', 'verbose output'],
  debug : ['d', 'debug output']
});

databaseInitialize()

const appStartTime = new Date().getTime();

const app_name = 'AlaSQL Benchmark'
const app_version = '1.0.0';

cli.setApp( app_name, app_version );
cli.enable( 'status', 'version' );

function databaseInitialize()
{
  alasql('CREATE TABLE shop (id BIGINT PRIMARY KEY AUTOINCREMENT, name STRING)')
  alasql('CREATE TABLE category (id BIGINT PRIMARY KEY AUTOINCREMENT, name STRING, ref_parent_category BIGINT REFERENCES category(id) DEFAULT NULL)')
  alasql('CREATE TABLE product (id BIGINT PRIMARY KEY AUTOINCREMENT, name STRING, ean STRING, manufacturer_code STRING, manufacturer_name STRING, ref_category BIGINT REFERENCES category(id))')
  alasql('CREATE TABLE offer (id BIGINT AUTOINCREMENT, date DATETIME, ref_product BIGINT REFERENCES product(id), price MONEY, ref_shop BIGINT REFERENCES shop(id))')
  for(i = 0; i < options.loops; i++) alasql('INSERT INTO shop (name) VALUES ("INITVALUE")')
  alasql.promise([
                      'SELECT * INTO category FROM CSV("/home/jjkdev/Desktop/dataset/with_headers/categories.csv", {headers:true, separator:","})'
                    , 'SELECT * INTO product FROM CSV("/home/jjkdev/Desktop/dataset/with_headers/products.csv", {headers:true, separator:","})'
                    , 'SELECT * INTO offer FROM CSV("/home/jjkdev/Desktop/dataset/with_headers/offers.csv", {headers:true, separator:","})'
                ])
              .then(function(){

                let time = new Date().getTime() - appStartTime;

                if( time > 0 ) cli.info( 'Database import time: ' + time + ' ms' )

                main()

              }).catch(function(err){ cli.fatal( err ) });
}

function main()
{
  // Check if database has been initialized correctly
  let categories = alasql('SELECT * FROM category')
  let shops = alasql('SELECT * FROM shop')
  let products = alasql('SELECT * FROM product')
  let offers = alasql('SELECT * FROM offer')

  if( !(categories.length > 0 && shops.length > 0 && products.length > 0 && offers.length > 0)) cli.fatal( 'One of the imported tables is empty.' )
  else cli.ok( 'Database has been verified.' )

  let startTime;

  let price = rand(100, 2000);
  let offer_id = rand(100, 2000);
  let shop_id = 312;
  let date = '2016-01-01';
  let shop_name = 'RTV EURO AGD';
  let category_name = 'Ekspresy do kawy';
  let manufacturer_name = 'De Longhi';

  switch( options.operation )
  {
  case 'read_all':

    startTime = new Date().getTime()

    for(i = 0; i < options.loops; i++)
    {
       let result = alasql('SELECT * FROM offer')
    }

  break;
  case 'read1w':

    startTime = new Date().getTime()

    for(i = 0; i < options.loops; i++)
    {
      let result = alasql('SELECT * FROM offer WHERE price >' + price)
    }

  break;
  case 'read2w':

    startTime = new Date().getTime()

    for(i = 0; i < options.loops; i++)
    {
      let result = alasql('SELECT * FROM offer WHERE price > ' + price + ' AND date > "' + date + '"')
    }
  break;
  case 'read3w':

    startTime = new Date().getTime()

    for(i = 0; i < options.loops; i++)
    {
      let result = alasql('SELECT * FROM offer WHERE price > ' + price + ' AND date > "' + date + '" AND ref_shop = ' + shop_id)
    }

  break;
  case 'read1wj':

    startTime = new Date().getTime()

    for(i = 0; i < options.loops; i++)
    {
      let result = alasql('SELECT * FROM offer AS o JOIN shop AS s ON s.id = o.ref_shop WHERE s.name = "' + shop_name + '"')
    }

  break;
  case 'read2wj':

    startTime = new Date().getTime()

    for(i = 0; i < options.loops; i++)
    {
      let result = alasql('SELECT * FROM offer AS o JOIN shop AS s ON s.id = o.ref_shop JOIN product AS p ON p.id = o.ref_product WHERE s.name = "'+shop_name+'" AND p.manufacturer_name = "'+manufacturer_name+'"')
    }

  break;
  case 'read3wj':

    startTime = new Date().getTime()

    for(i = 0; i < options.loops; i++)
    {
      let result = alasql('SELECT * FROM offer AS o JOIN shop AS s ON s.id = o.ref_shop JOIN product AS p ON p.id = o.ref_product JOIN category AS c ON c.id = p.ref_category WHERE s.name = "'+shop_name+'" AND p.manufacturer_name = "'+manufacturer_name+'" AND c.name = "'+category_name+'"')
    }

  break;
  case 'insert':

    startTime = new Date().getTime()
    for(i = 0; i < options.loops; i++) alasql.tables.shop.data.push( [100000+i, 'TEST'] )

  break;
  case 'update':

    startTime = new Date().getTime()
    alasql('UPDATE shop SET name = "' + shop_name + '" WHERE id <= "' + options.loops + '"');

  break;
  case 'delete':

    let indexes = []
    for(i = 0; i < alasql.tables.offer.data.length; i++)
    {
        indexes.push( alasql.tables.offer.data[i].id )
    }

    startTime = new Date().getTime();

    for(i = 0; i < options.loops; i++)
    {
      alasql('DELETE FROM offer WHERE id = '+indexes[i])
    }

  break;
  }

  let totalTime = logTime( startTime )
  show_result( totalTime )

  process.exit()
}

function show_result( totalTime )
{
  console.log('');
  console.log('Operation (ms): ' + totalTime/options.loops);
  console.log('');
  console.log('Total Time: ' + totalTime);
  console.log('Total Operations: ' + options.loops);
  console.log('');
  console.log('TPS (true): ' +  ((options.loops/totalTime)*1000) );
}

function logTime( startTime )
{
  let endTime = new Date().getTime();
  let totalTime = endTime - startTime;

  return totalTime;
}

function rand(min, max) { return Math.floor(Math.random() * (Math.floor(max) - Math.ceil(min))) + Math.ceil(min); }
