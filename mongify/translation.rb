table "shops" do
  column "id", :key, :as => :integer
  column "name", :string
end

table "categories" do
  column "id", :key, :as => :integer
  column "name", :string
  column "ref_parent_category", :integer, :references => :categories
end

table "products" do
  column "id", :key, :as => :integer
  column "name", :string
  column "ean", :string
  column "manufacturer_code", :string
  column "manufacturer_name", :string
  column "ref_category", :integer, :references => :categories
end

table "offers" do
  column "id", :key, :as => :integer
  column "ref_product", :integer, :references => :products
  column "price", :float
  column "ref_shop", :integer, :references => :shops
  column "date", :datetime
end
