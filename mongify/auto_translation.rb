table "categories" do
	column "id", :key, :as => :integer
	column "name", :string
	column "ref_parent_category", :integer
end

table "offers" do
	column "id", :key, :as => :integer
	column "ref_product", :integer
	column "price", :decimal
	column "shop", :string
	column "ref_shop", :integer
	column "date", :datetime
end

table "products" do
	column "id", :key, :as => :integer
	column "name", :string
	column "ean", :string
	column "manufacturer_code", :string
	column "manufacturer_name", :string
	column "ref_category", :integer
end

table "shops" do
	column "id", :key, :as => :integer
	column "name", :string
end

