\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Introduction}{5}
\contentsline {section}{\numberline {1.1}Problem Statement}{5}
\contentsline {section}{\numberline {1.2}Goal and Scope}{5}
\contentsline {section}{\numberline {1.3}Related Works}{5}
\contentsline {subsubsection}{\IeC {\'Z}r\IeC {\'o}d\IeC {\l }a ksi\IeC {\k a}\IeC {\.z}kowe polskoj\IeC {\k e}zyczne i t\IeC {\l }umaczenia}{6}
\contentsline {subsubsection}{\IeC {\'Z}r\IeC {\'o}d\IeC {\l }a ksi\IeC {\k a}\IeC {\.z}kowe obcoj\IeC {\k e}zyczne}{6}
\contentsline {subsubsection}{Artyku\IeC {\l }y naukowe, raporty z bada\IeC {\'n}, komunikaty konferencyjne, dokumentacje techniczne, manuale, instrukcje}{6}
\contentsline {subsubsection}{\IeC {\'Z}r\IeC {\'o}d\IeC {\l }a elektroniczne}{6}
\contentsline {section}{\numberline {1.4}Document Structure}{6}
\contentsline {chapter}{\numberline {2}Relational and Non-relational In-memory Databases}{7}
\contentsline {section}{\numberline {2.1}Basic Concepts and Definitions}{7}
\contentsline {subsection}{\numberline {2.1.1}Definitions}{8}
\contentsline {section}{\numberline {2.2}Relational Databases}{8}
\contentsline {section}{\numberline {2.3}Non-relational Databases}{8}
\contentsline {section}{\numberline {2.4}In-Memory Databases}{8}
\contentsline {chapter}{\numberline {3}Experimental Studies}{9}
\contentsline {section}{\numberline {3.1}Methodology Overview}{9}
\contentsline {subsection}{\numberline {3.1.1}Research Purpose}{10}
\contentsline {subsection}{\numberline {3.1.2}Method Description}{11}
\contentsline {subsection}{\numberline {3.1.3}Dataset}{13}
\contentsline {subsubsection}{Relational Model}{16}
\contentsline {subsubsection}{Document Store Model}{16}
\contentsline {subsubsection}{Key-Value Store Model}{16}
\contentsline {section}{\numberline {3.2}Evaluated Databases}{16}
\contentsline {subsection}{\numberline {3.2.1}VoltDB}{16}
\contentsline {subsection}{\numberline {3.2.2}AlaSQL}{16}
\contentsline {subsection}{\numberline {3.2.3}ArangoDB}{16}
\contentsline {subsection}{\numberline {3.2.4}MongoDB (Percona Server)}{16}
\contentsline {subsection}{\numberline {3.2.5}LokiJS}{16}
\contentsline {section}{\numberline {3.3}Environment and Tools}{17}
\contentsline {subsection}{\numberline {3.3.1}Testing Environment}{17}
\contentsline {subsection}{\numberline {3.3.2}Atom.io}{17}
\contentsline {subsection}{\numberline {3.3.3}SQL}{18}
\contentsline {subsection}{\numberline {3.3.4}AQL}{18}
\contentsline {subsection}{\numberline {3.3.5}JAVA}{18}
\contentsline {subsection}{\numberline {3.3.6}JavaScript}{19}
\contentsline {subsection}{\numberline {3.3.7}NodeJS}{20}
\contentsline {subsection}{\numberline {3.3.8}VoltDB Driver}{20}
\contentsline {subsection}{\numberline {3.3.9}MySQL Driver}{20}
\contentsline {subsection}{\numberline {3.3.10}ArangoDB Driver}{20}
\contentsline {subsection}{\numberline {3.3.11}MongoDB Driver}{20}
\contentsline {subsection}{\numberline {3.3.12}LokiJS Driver}{20}
\contentsline {chapter}{\numberline {4}Experimental Results}{21}
\contentsline {section}{\numberline {4.1}Results of Data Import Operations}{22}
\contentsline {section}{\numberline {4.2}Results of SELECT Queries}{22}
\contentsline {section}{\numberline {4.3}Results of SELECT all Queries}{22}
\contentsline {section}{\numberline {4.4}Results SELECT with WHERE Condition Queries}{22}
\contentsline {section}{\numberline {4.5}Results SELECT with WHERE and JOIN Statements Queries}{22}
\contentsline {section}{\numberline {4.6}Results of INSERT n Records Queries}{22}
\contentsline {section}{\numberline {4.7}Results of DELETE n Records Queries}{22}
\contentsline {section}{\numberline {4.8}Results of DELETE all Records Queries}{22}
\contentsline {section}{\numberline {4.9}Results Discussion}{22}
\contentsline {chapter}{\numberline {5}Conclusions}{23}
\contentsline {section}{\numberline {5.1}Summary}{23}
\contentsline {section}{\numberline {5.2}Further Development}{23}
\contentsline {chapter}{Bibliography}{23}
\contentsline {chapter}{List of Figures}{24}
\contentsline {chapter}{List of Tables}{25}
\contentsline {chapter}{Appendix:}{}
\contentsline {chapter}{\numberline {A}Benchmark Database Schema}{27}
