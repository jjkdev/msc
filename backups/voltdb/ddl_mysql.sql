--Clear remnants of ceneo database
DROP TABLE shop IF EXISTS;
DROP TABLE category IF EXISTS;
DROP TABLE product IF EXISTS;
DROP TABLE offer IF EXISTS;

--Load schema
CREATE TABLE shop
(
  id BIGINT NOT NULL
, name VARCHAR(128) NOT NULL
, PRIMARY KEY (id)
);

CREATE TABLE category
(
  id BIGINT NOT NULL
, name VARCHAR(256) NOT NULL
, ref_parent_category BIGINT DEFAULT NULL
, PRIMARY KEY (id)
);

CREATE TABLE product
(
  id BIGINT NOT NULL
, name VARCHAR(256) NOT NULL
, ean VARCHAR(32) DEFAULT NULL
, manufacturer_code VARCHAR(256) DEFAULT NULL
, manufacturer_name VARCHAR(256) DEFAULT NULL
, ref_category BIGINT DEFAULT NULL
, get_offers TINYINT DEFAULT 0
, PRIMARY KEY (id)
);

CREATE TABLE offer
(
  id BIGINT NOT NULL
, ref_api_id BIGINT NOT NULL
, ref_product BIGINT NOT NULL
, price DECIMAL(20,2) NOT NULL
, shop VARCHAR(128) NOT NULL
, ref_shop BIGINT DEFAULT NULL
, date TIMESTAMP NOT NULL
, PRIMARY KEY (id)
);
