--Clear remnants of ceneo database
DROP TABLE shop IF EXISTS;
DROP TABLE category IF EXISTS;
DROP TABLE product IF EXISTS;
DROP TABLE offer IF EXISTS;
--Clear remnants of employee database
DROP TABLE employee IF EXISTS;
DROP TABLE salaries IF EXISTS;
DROP TABLE titiles IF EXISTS;
DROP TABLE departments IF EXISTS;
DROP TABLE dept_manager IF EXISTS;
DROP TABLE dept_emp IF EXISTS;
--Load schema
CREATE TABLE IDENTIFIER
(
  TABLE_NAME VARCHAR(100) NOT NULL
, CURRENT_VALUE INTEGER DEFAULT 1 NOT NULL
, PRIMARY KEY (TABLE_NAME)
);

CREATE TABLE departments
(
  dept_no VARCHAR(4) NOT NULL
, dept_name VARCHAR(32) NOT NULL
, PRIMARY KEY (dept_no)
);

CREATE TABLE dept_emp
(
  emp_no INT NOT NULL
, dept_no VARCHAR(4) NOT NULL
, from_date TIMESTAMP NOT NULL
, to_date TIMESTAMP NOT NULL
, CONSTRAINT pk_dept_emp PRIMARY KEY (emp_no, dept_no)
);

CREATE TABLE dept_manager
(
  emp_no INT NOT NULL
, dept_no VARCHAR(4) NOT NULL
, from_date TIMESTAMP NOT NULL
, to_date TIMESTAMP NOT NULL
, CONSTRAINT pk_dept_manager PRIMARY KEY (emp_no, dept_no)
);

CREATE TABLE employees
(
  emp_no INT NOT NULL
, birth_date TIMESTAMP NOT NULL
, first_name VARCHAR(14) NOT NULL
, last_name VARCHAR(16) NOT NULL
, gender VARCHAR(1) NOT NULL
, hire_date TIMESTAMP NOT NULL
, PRIMARY KEY (emp_no)
);

CREATE TABLE salaries
(
  emp_no INT NOT NULL
, salary INT NOT NULL
, from_date TIMESTAMP NOT NULL
, to_date TIMESTAMP NOT NULL
, CONSTRAINT pk_salary_from_date PRIMARY KEY (emp_no, from_date)
);

CREATE TABLE titles
(
  emp_no INT NOT NULL
, title VARCHAR(50) NOT NULL
, from_date TIMESTAMP NOT NULL
, to_date TIMESTAMP NOT NULL
, CONSTRAINT pk_title_from_date PRIMARY KEY (emp_no, title, from_date)
);
