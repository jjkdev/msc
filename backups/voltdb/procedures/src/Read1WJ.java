import org.voltdb.*;

public class Read1WJ extends VoltProcedure
{

	public final SQLStmt query = new SQLStmt(
		"SELECT * FROM offer AS o JOIN shop AS s ON s.id = o.ref_shop WHERE s.name = ?;"
	);

	public VoltTable[] run(String shop_name) throws VoltAbortException
  {

		voltQueueSQL( query, shop_name );
		VoltTable[] results = voltExecuteSQL();

		return results;
	}

}
