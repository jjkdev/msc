import org.voltdb.*;

public class Delete extends VoltProcedure
{

	public final SQLStmt query = new SQLStmt(
		"DELETE FROM offer ORDER BY id ASC LIMIT ?;"
	);

	public VoltTable[] run(int limit) throws VoltAbortException
  {

		voltQueueSQL( query, limit);
		VoltTable[] results = voltExecuteSQL();

		return results;
	}

}
