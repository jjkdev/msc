import org.voltdb.*;

public class Read1W extends VoltProcedure
{

	public final SQLStmt query = new SQLStmt(
		"SELECT * FROM OFFER WHERE price > ?;"
	);

	public VoltTable[] run(double price) throws VoltAbortException
  {

		voltQueueSQL( query, price );
		VoltTable[] results = voltExecuteSQL();

		return results;
	}

}
