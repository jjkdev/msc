import org.voltdb.*;

public class ReadAll extends VoltProcedure
{

	public final SQLStmt query = new SQLStmt(
		"SELECT * FROM offer ORDER BY id ASC LIMIT ? OFFSET ?;"
	);

	public VoltTable[] run(int limit, int offset) throws VoltAbortException
  {

		voltQueueSQL( query, limit, offset);
		VoltTable[] results = voltExecuteSQL();

		return results;
	}

}
