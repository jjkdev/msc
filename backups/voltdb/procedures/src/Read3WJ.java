import org.voltdb.*;

public class Read3WJ extends VoltProcedure
{

	public final SQLStmt query = new SQLStmt(
		"SELECT * FROM offer AS o JOIN shop AS s ON s.id = o.ref_shop JOIN product AS p ON p.id = o.ref_product JOIN category AS c ON c.id = p.ref_category WHERE s.name = ? AND p.manufacturer_name = ? AND c.name = ?;"
	);

	public VoltTable[] run(String shop_name, String manufacturer_name, String category_name) throws VoltAbortException
  {

		voltQueueSQL( query, shop_name, manufacturer_name, category_name );
		VoltTable[] results = voltExecuteSQL();

		return results;
	}

}
