import org.voltdb.*;

public class Write extends VoltProcedure
{

	public final SQLStmt query = new SQLStmt(
		"INSERT INTO shop VALUES (?, ?);"
	);

	public VoltTable[] run(long id, String name) throws VoltAbortException
  {

		voltQueueSQL( query, id, name);
		VoltTable[] results = voltExecuteSQL();

		return results;
	}

}
