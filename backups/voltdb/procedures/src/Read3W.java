import org.voltdb.*;
import org.voltdb.types.TimestampType;

public class Read3W extends VoltProcedure
{

	public final SQLStmt query = new SQLStmt(
		"SELECT * FROM OFFER WHERE price > ? AND date > ? AND ref_shop = ?;"
	);

	public VoltTable[] run(double price, long date, long shop_id) throws VoltAbortException
  {
		voltQueueSQL( query, price, new TimestampType(date), shop_id);
		VoltTable[] results = voltExecuteSQL();

		return results;
	}

}
