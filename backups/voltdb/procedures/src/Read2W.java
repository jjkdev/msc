import org.voltdb.*;
import org.voltdb.types.TimestampType;

public class Read2W extends VoltProcedure
{

	public final SQLStmt query = new SQLStmt(
		"SELECT * FROM OFFER WHERE price > ? AND date > ?;"
	);

	public VoltTable[] run(double price, long date) throws VoltAbortException
  {
		voltQueueSQL( query, price, new TimestampType(date) );
		VoltTable[] results = voltExecuteSQL();

		return results;
	}

}
