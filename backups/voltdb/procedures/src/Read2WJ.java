import org.voltdb.*;

public class Read2WJ extends VoltProcedure
{

	public final SQLStmt query = new SQLStmt(
		"SELECT * FROM offer AS o JOIN shop AS s ON s.id = o.ref_shop JOIN product AS p ON p.id = o.ref_product WHERE s.name = ? AND p.manufacturer_name = ?;"
	);

	public VoltTable[] run(String shop_name, String manufacturer_name) throws VoltAbortException
  {

		voltQueueSQL( query, shop_name, manufacturer_name );
		VoltTable[] results = voltExecuteSQL();

		return results;
	}

}
