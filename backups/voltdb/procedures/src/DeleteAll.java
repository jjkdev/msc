import org.voltdb.*;

public class DeleteAll extends VoltProcedure
{

	public final SQLStmt query = new SQLStmt(
		"DELETE FROM offer;"
	);

	public VoltTable[] run() throws VoltAbortException
  {

		voltQueueSQL( query );
		VoltTable[] results = voltExecuteSQL();

		return results;
	}

}
