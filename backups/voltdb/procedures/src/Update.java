import org.voltdb.*;

public class Update extends VoltProcedure
{

	public final SQLStmt query = new SQLStmt(
		"UPDATE offer SET price = ? WHERE id <= ?;"
	);

	public VoltTable[] run(double price, int id) throws VoltAbortException
  {

		voltQueueSQL( query, price, id);
		VoltTable[] results = voltExecuteSQL();

		return results;
	}

}
