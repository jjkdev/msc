import org.voltdb.*;

public class Read1W extends VoltProcedure
{

	public final SQLStmt query = new SQLStmt(
		"SELECT * FROM EMPLOYEE WHERE gender = ?;"
	);

	public VoltTable[] run(String gender) throws VoltAbortException
  {

		voltQueueSQL( query, gender );
		VoltTable[] results = voltExecuteSQL();

		return results;
	}

}
