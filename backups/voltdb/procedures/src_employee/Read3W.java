import org.voltdb.*;

public class Read2W extends VoltProcedure
{

	public final SQLStmt query = new SQLStmt(
		"SELECT * FROM EMPLOYEE WHERE gender = ? AND birth_date > ?;"
	);

	public VoltTable[] run(String gender, String birth_date) throws VoltAbortException
  {

		voltQueueSQL( query, gender, birth_date );
		VoltTable[] results = voltExecuteSQL();

		return results;
	}

}
