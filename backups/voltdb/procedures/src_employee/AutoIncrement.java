import org.voltdb.*;

public class AutoIncrement extends VoltProcedure
{

	public final SQLStmt select_id = new SQLStmt(
		"SELECT CURRENT_VALUE FROM IDENTIFIER WHERE TABLE_NAME = ?"
	);

	public final SQLStmt increment_id = new SQLStmt(
		"UPDATE IDENTIFIER SET CURRENT_VALUE = CURRENT_VALUE + 1 " +
		"WHERE TABLE_NAME = ?"
	);

	public VoltTable[] run(String tableName) throws VoltAbortException
  {
		voltQueueSQL(select_id, tableName);
		VoltTable[] id = voltExecuteSQL();

		voltQueueSQL(increment_id, tableName);
		voltExecuteSQL(true);

		return id;
	}

}
