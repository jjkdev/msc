import org.voltdb.*;

public class AddTableAutoIncrement extends VoltProcedure
{

	public final SQLStmt addAutoIncrement = new SQLStmt(
		"INSERT INTO IDENTIFIER (TABLE_NAME) VALUES (?)"
	);

	public VoltTable[] run (String tableName) throws VoltAbortException
  {
  	voltQueueSQL(addAutoIncrement, tableName);
  	voltExecuteSQL(true);
  	return null;
  }

}
