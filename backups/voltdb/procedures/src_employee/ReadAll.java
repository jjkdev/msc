import org.voltdb.*;

public class ReadAll extends VoltProcedure
{

	public final SQLStmt query = new SQLStmt(
		"SELECT * FROM EMPLOYEE;"
	);

	public VoltTable[] run() throws VoltAbortException
  {

		voltQueueSQL(query);
		VoltTable[] results = voltExecuteSQL();

		return results;
	}

}
