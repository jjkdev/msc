\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Introduction}{4}
\contentsline {section}{\numberline {1.1}Problem Statement}{4}
\contentsline {section}{\numberline {1.2}Purpose and Scope of Work}{5}
\contentsline {section}{\numberline {1.3}Paper Structure}{6}
\contentsline {chapter}{\numberline {2}Relational and Non-relational Databases}{8}
\contentsline {section}{\numberline {2.1}Relational Model}{8}
\contentsline {subsubsection}{Codd's 12 Rules}{8}
\contentsline {subsubsection}{Terminology}{12}
\contentsline {subsubsection}{Keys}{13}
\contentsline {section}{\numberline {2.2}Relational Databases}{14}
\contentsline {section}{\numberline {2.3}Non-relational Databases}{15}
\contentsline {section}{\numberline {2.4}In-Memory Databases}{17}
\contentsline {chapter}{\numberline {3}Experimental Studies}{19}
\contentsline {section}{\numberline {3.1}Methodology Overview}{19}
\contentsline {subsection}{\numberline {3.1.1}Research Purpose}{20}
\contentsline {subsection}{\numberline {3.1.2}Method Description}{21}
\contentsline {subsection}{\numberline {3.1.3}Dataset}{24}
\contentsline {subsection}{\numberline {3.1.4}Relational Model}{27}
\contentsline {subsection}{\numberline {3.1.5}Document Store Model}{27}
\contentsline {section}{\numberline {3.2}Evaluated Databases}{29}
\contentsline {subsection}{\numberline {3.2.1}VoltDB}{29}
\contentsline {subsection}{\numberline {3.2.2}AlaSQL}{30}
\contentsline {subsection}{\numberline {3.2.3}MongoDB (Percona Server)}{31}
\contentsline {subsection}{\numberline {3.2.4}LokiJS}{32}
\contentsline {section}{\numberline {3.3}Environment and Tools}{33}
\contentsline {subsection}{\numberline {3.3.1}Testing Environment}{33}
\contentsline {subsection}{\numberline {3.3.2}Atom.io}{34}
\contentsline {subsection}{\numberline {3.3.3}SQL}{34}
\contentsline {subsection}{\numberline {3.3.4}Java}{37}
\contentsline {subsection}{\numberline {3.3.5}JavaScript}{38}
\contentsline {subsection}{\numberline {3.3.6}Node.js}{38}
\contentsline {subsection}{\numberline {3.3.7}VoltDB Driver}{38}
\contentsline {subsection}{\numberline {3.3.8}MongoDB Driver}{40}
\contentsline {chapter}{\numberline {4}Experimental Results}{42}
\contentsline {section}{\numberline {4.1}Measurements Results}{42}
\contentsline {section}{\numberline {4.2}Results Discussion}{45}
\contentsline {chapter}{\numberline {5}Conclusions}{51}
\contentsline {section}{\numberline {5.1}Summary}{51}
\contentsline {section}{\numberline {5.2}Further Development}{53}
\contentsline {chapter}{Bibliography}{53}
\contentsline {chapter}{List of Figures}{57}
\contentsline {chapter}{List of Tables}{58}
